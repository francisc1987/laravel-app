<?php

use App\Trip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
/*
 * TRIPS
 */
Route::get('trips', 'TripController@index');
Route::get('trips/collection', 'TripController@collection');
Route::get('trips/{id}', function($id) {
    return response()->json(Trip::find($id), 200);
});
Route::get('trips/slug/{slug}', 'TripController@slug');
Route::post('trips/create', 'TripController@create');
Route::delete('trips/{id}', 'TripController@destroy');
Route::post('trips/filter/', 'TripController@filter');
Route::post('trips/update/{id}', 'TripController@update');
Route::group(['middleware' => 'auth:api'], function() {
    Route::post('bookings/', 'TripUserController@create');
    Route::get('bookings/', 'TripUserController@index');
});

/*
 * USERS
 */
Route::post('user/register', 'UserController@register');
Route::post('user/login', 'UserController@login');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('user/details', 'UserController@details');
    Route::post('user/logout', 'UserController@logout');
    Route::put('user/update', 'UserController@update');
    Route::delete('user/delete', 'UserController@delete');
});
