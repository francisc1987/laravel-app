<?php

use App\Trip;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Trip::truncate();
        $faker = \Faker\Factory::create();
        for ($i = 1; $i <= 50; $i++) {
            $start_date = $faker->dateTimeBetween('now', '+' . rand(1,30) . ' days');
            $start_date_clone = clone $start_date;
            $end_date = $start_date_clone->modify('+' . rand(1,30) . ' days');
            $title = str_ireplace('.', '', $faker->sentence);
            Trip::create([
                'title' => $title,
                'description' => $faker->paragraph,
                'location' => $faker->city,
                'price' => $faker->randomFloat(2,50, 1000),
                'start_date' => $start_date,
                'end_date' => $end_date,
                'slug' => Str::slug($title) . '-' . $i,
            ]);
        }
    }
}
