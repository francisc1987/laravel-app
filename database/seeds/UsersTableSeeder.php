<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $faker = \Faker\Factory::create();
        $password = 'lara1234';
        $first_name = $faker->firstName;
        $last_name = $faker->lastName;
        for ($i = 1; $i <= 50; $i++) {
            $user = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $faker->email,
                'password' => bcrypt($password),
            ]);
            $user->createToken('MyApp')->accessToken;
        }
    }
}
