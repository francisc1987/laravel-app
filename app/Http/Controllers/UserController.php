<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use ThrottlesLogins, AuthenticatesUsers;
    public $successStatus = 200;

    protected $maxAttempts = 5; // Amount of bad attempts user can make
    protected $decayMinutes = 1;

    /**
     * login api
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }

        $this->incrementLoginAttempts($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            return response()->json(['error' => 'to many failed attempts in one minute'], 401);
        }
        return response()->json(['error' => 'invalid credentials'], 400);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 404);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['first_name'] =  $user->first_name;
        $success['last_name'] =  $user->last_name;
        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * details api
     *
     *@return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function logout()
    {
        $user = Auth::user()->token();
        $user->revoke();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $allowedAttributesKeyArr = ['first_name', 'last_name', 'email'];
        foreach (array_keys($request->all()) as $attributeKey ) {
            if(!in_array($attributeKey, $allowedAttributesKeyArr, true)) {
                return response()->json(['error'=>'parameter ' . $attributeKey . ' is not allowed to be updated'], 401);
            }
        }
        $user = Auth::user();
        $input = $request->all();
        if(isset($input['password'])) {
            return response()->json('you are not allowed to reset password', 400);
        }
        $user->update($input);
        return response()->json(['success' => $user], $this->successStatus);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        $user = Auth::user();
        $id = $user->id;
        $userTokens = $user->tokens;
        if(User::destroy($id)) {
            foreach($userTokens as $token) {
                $token->revoke();
            }
            return response()->json(['success' => $user], $this->successStatus);
        }

        return response()->json(['error' => 'user could not be deleted'], 400);
    }

}
