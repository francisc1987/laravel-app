<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TripController extends Controller
{
    protected $successStatus = 200;

    /**
     * Display a listing of the trips.
     *
     * @return \App\Trip[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Trip::all();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection(Request $request)
    {
        $tripTable = new Trip();
        $columnNamesArr = $tripTable->getTableColumns();
        foreach ($request->all()['params'] as $paramsValue) {
            if (!in_array($paramsValue, $columnNamesArr, true)) {
                return response()->json('the parameter value ' . $paramsValue . ' is not accepted', 404);
            }
        }
        $collection = collect(Trip::all()->toArray())->map(function ($item) use ($request) {
            return collect($item)->only($request->all()['params']);
        });
        return response()->json($collection, 200);
    }

    /**
     * Create new trip.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'location' => 'required',
            'price' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 404);
        }
        $input = $request->all();
        $trip = Trip::create($input);
        $trip->slug = Str::slug($trip->title) . '-' . $trip->id;
        $trip->save();
        return response()->json(['success'=>$trip], $this->successStatus);
    }

    /**
     * Update the specified trip.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Trip::findOrFail($id);
        $article->update($request->all());

        return $article;
    }

    /**
     * Fetch record matching slug from trips.
     *
     * @param $slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function slug($slug)
    {
        $trip = Trip::where('slug', '=', $slug)->firstOrFail();
        return response()->json(['success'=>$trip], 200);
    }

    /**
     * Remove the specified trip.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip = Trip::find($id)->delete();

        return response()->json(['record with id ' . $id. ' was deleted'], 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Trip $trip
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request, Trip $trip)
    {
        $trip = $trip->newQuery();
        if($request->has('orderby')) {
            $trip->orderBy($request->input('orderby'), 'asc');
        }
        if($request->has('search')) {
            $trip->orWhere('title', 'LIKE', '%' . $request->input('search') . '%');
            $trip->orWhere('description', 'LIKE', '%' . $request->input('search') . '%');
            $trip->orWhere('location', 'LIKE', '%' . $request->input('search') . '%');
        }
        if($request->has('priceto') && !$request->has('pricefrom')) {
            $trip->where('price', '<=' ,$request->input('priceto'));
        }
        if($request->has('pricefrom') && $request->has('priceto')) {
            $trip->whereBetween('price', [$request->input('pricefrom') ,$request->input('priceto')]);
        }
        if($request->has('pricefrom') && !$request->has('priceto')) {
            $trip->orWhere('price', '>=' ,$request->input('pricefrom'));
        }
        return response()->json(['success' => $trip->get()], 200);
    }
}
