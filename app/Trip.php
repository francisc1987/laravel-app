<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Trip extends Model
{
    protected $fillable = ['id','title', 'description', 'location', 'price', 'start_date', 'end_date', 'slug'];

    /**
     * @return array
     */
    public function getTableColumns() : array {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    protected static function boot() {
        parent::boot();

    }

    /**
     * The users that belong to the trip.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'trip_user');
    }
}
